#include <iostream>

using namespace std;
void increment(int *p)
{
	*p = (*p) + 1;
}

int main()
{
	int a;
	a = 10;
	increment(&a);
	cout << "Adress of a in main is : " << &a << endl;
	cout << a << endl;
	system("pause");
	return 0;
}