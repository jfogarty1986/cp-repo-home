//Child class
#ifndef ISOSCELES_H
#define ISOSCELES_H

#include <iostream>
#include "Triangle.h"
using namespace std;

class Isosceles : public Triangle
{
public:
	//Default Constructor
	Isosceles();

	//Overload Constructor
	Isosceles(int, int, int);

	//Accessor Function
	int getBase() const;

	int getSideOne() const;

	//Mutator Function
	void setBase(int);

	void setSideOne(int);

	//Destructor
	~Isosceles();

	int getPerimeter() const;

	void printInfo() const;
private:
	//Member variables
	int base;
	int sideOne;

};

#endif // !ISOSCELES_H

