#include <iostream>
#include "Isosceles.h"

using namespace std;

int main()
{
	int base, height, equalSide;

	cout << "Enter base length : ";
	cin >> base;
	cout << "Enter height : ";
	cin >> height;
	cout << "type in side that is equal : ";
	cin >> equalSide;
	

	Isosceles myTriangle(base, height, equalSide);
	cout << endl;
	myTriangle.printInfo();


	system("pause");
	return 0;
}