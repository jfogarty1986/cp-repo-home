//base parent class
#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <iostream>

using namespace std;

class Triangle
{
public:
	//Deafult Constructor
	Triangle();
	//Overload Constructor
	Triangle(int);
	//Accessor function
	int getHeight() const;
	//Mutator function
	void setHeight(int);
	//Destructer
	~Triangle();
	//Get area
	double getArea(int) const;
private:
	int height;
};
#endif // !TRIANGLE_H


