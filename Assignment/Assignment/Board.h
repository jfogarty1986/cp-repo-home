#pragma once
#include "Game_Object.h"
#include "Player.h"
#include "Enemy.h"
#include <iostream>
#include <string>
#define SIZE 10

using namespace std;

class Board
{
public:
	Board();
	~Board();
	void update_board(Player*,Enemy*,Enemy*,Enemy*,Enemy*);
	void print_board();

private:
	char board[SIZE][SIZE];
};

