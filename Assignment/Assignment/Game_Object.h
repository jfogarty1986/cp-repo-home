#ifndef DEFINE GAME_OBJECT_H
#define GAME_OBJECT_H



class Game_Object
{
public:
	Game_Object();
	~Game_Object();
	bool isAlive();
	int get_X();
	int get_Y();
	char set_Type(char);
private:
	int m_Health;
	int m_X;
	int m_Y;
	char m_TypeID;
};

#endif // !1