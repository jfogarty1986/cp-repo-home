#include <iostream>
#include <ctime>
#include <algorithm>
#include <vector>
#include <string>
#include <conio.h>
#include "Enemy.h"
#include "Player.h"
#define EMPTY '.'
#define MAX 20

using namespace std;
void fill_array(char[MAX][MAX], Player*, Enemy*, Enemy*, Enemy*, Enemy*);
void print_board(char board[MAX][MAX]);
void health(Player*, Enemy*, Enemy*, Enemy*, Enemy*);
void collision(Player*, Enemy*, Enemy*, Enemy*, Enemy*);
void battle(Player *player, Enemy *enemy);
void welcome();
char random_move();





int main()
{
	srand(static_cast<unsigned int>(time(NULL))); // Seeds a random number
	char board[MAX][MAX],move;
	int speed = 0;
	Player player;
	Enemy enemy1('1'), enemy2('2'), enemy3('3'), enemy4('4');
	Player *p_player = &player;
	Enemy *p_enemy1 = &enemy1;
	Enemy *p_enemy2 = &enemy2;
	Enemy *p_enemy3 = &enemy3;
	Enemy *p_enemy4 = &enemy4;
	fill_array(board, p_player, p_enemy1, p_enemy2, p_enemy3, p_enemy4);
	welcome();
	print_board(board);
	player.set_Speed(2);
	enemy1.set_Speed(4);
	enemy2.set_Speed(4);
	enemy3.set_Speed(4);
	enemy4.set_Speed(4);
	//cout << "Player loc : " << &player;
	//cout << "Player X : " << player.get_X();
	//cout << "Pointer player X : " << p_player->get_X();
	bool game_over = false;
	
	while (player.isAlive())
	{
		//cout << "Enter move : ";
		//move = _getch();
		player.update(random_move(), board);
		enemy1.update(random_move(), board);
		enemy2.update(random_move(), board);
		enemy3.update(random_move(), board);
		enemy4.update(random_move(), board);
		welcome();
		print_board(board);
		collision(&player, &enemy1,&enemy2, &enemy3, &enemy4);
		health(p_player,p_enemy1,p_enemy2,p_enemy3,p_enemy4);
		//cout << player.get_X() << endl;
		//cout << player.get_Y() << endl;
		//cout << p_player->get_X() << endl;
		//cout << p_player->get_Y() << endl;
		getchar();
	}
	
	
	print_board(board);
	
	system("pause");
	return 0;
}



void print_board(char board[MAX][MAX])
{
	for (int i = 0; i < MAX; i++)
	{
		cout << "\t\t";
		for (int j = 0; j < MAX; j++)
		{	
			std::cout << board[i][j] << "  ";
		}
		cout << "\n";
	}
}

void fill_array(char board[MAX][MAX], Player *player, Enemy* enemy1, Enemy* enemy2, Enemy* enemy3, Enemy* enemy4)
{
	for (int i = 0; i < MAX; i++)
	{
		cout << "\t\t";
		for (int j = 0; j < MAX; j++)
		{
			board[i][j] = EMPTY;
			board[player->get_X()][player->get_Y()] = player->get_TypeID();
			board[enemy1->get_X()][enemy1->get_Y()] = enemy1->get_TypeID();
			board[enemy2->get_X()][enemy2->get_Y()] = enemy2->get_TypeID();
			board[enemy3->get_X()][enemy3->get_Y()] = enemy3->get_TypeID();
			board[enemy4->get_X()][enemy4->get_Y()] = enemy4->get_TypeID();
		}
		cout << "\n";
	}
}

void welcome()
{
	cout << "\t\t###################################################################\n";
	cout << "\t\t###################################################################\n";
	
}

void health(Player* player, Enemy* enemy1, Enemy* enemy2, Enemy* enemy3, Enemy* enemy4)
{
	cout << "    Player HP : " << player->get_Health() << " -> Enemy1 HP : " << enemy1->get_Health() << " ->, Enemy2 HP : " << enemy2->get_Health() << ", -> Enemy3 HP : " << enemy3->get_Health() << ", -> Enemy4 HP : " << enemy4->get_Health() << endl;
	cout << "Player speed : " << player->get_Speed() << endl;
}

char random_move()
{
	vector<char> random_move;
	random_move.push_back('w');
	random_move.push_back('a');
	random_move.push_back('s');
	random_move.push_back('d');
	random_shuffle(random_move.begin(), random_move.end());
	return random_move[1];
}

void collision(Player *player, Enemy *enemy1,Enemy *enemy2,Enemy *enemy3,Enemy *enemy4)
{
	if ((player->get_X() && player->get_Y()) == (enemy1->get_X() && enemy1->get_Y()))
	{
		battle(player, enemy1);
	}
	else if ((player->get_X() && player->get_Y()) == (enemy2->get_X() && enemy2->get_Y()))
	{
		battle(player, enemy2);
	}
	else if ((player->get_X() && player->get_Y()) == (enemy3->get_X() && enemy3->get_Y()))
	{
		battle(player, enemy3);
	}
	else if ((player->get_X() && player->get_Y()) == (enemy4->get_X() && enemy4->get_Y()))
	{
		battle(player, enemy4);
	}
}

void battle(Player *player, Enemy *enemy)
{
	if (player->get_Health() > enemy->get_Health())
	{
		enemy->isAlive() == false;
	}
	else
	{
		player->isAlive() == false;
	}
}