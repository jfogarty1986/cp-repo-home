//base Class
#include "GameObject.h"
#include <ctime>
#include <algorithm>



//Default Constructor
GameObject::GameObject()
{
	m_Health = 160;
	m_X = rand() % MAX;
	m_Y = rand() % MAX;
	m_Speed = 1;
}

//Overload Constructor
GameObject::GameObject(char newTypeID, int newHealth, int newX, int newY, int newSpeed)
{
	m_TypeID = newTypeID;
	m_Health = newHealth;
	m_X = newX;
	m_Y = newY;
	m_Speed = newSpeed;
}

//Accessor Functions
char GameObject::get_TypeID() const
{
	return m_TypeID;
}

int GameObject::get_Health() const
{
	return m_Health;
}

int GameObject::get_X() const
{
	return m_X;
}

int GameObject::get_Y() const
{
	return m_Y;
}

int GameObject::get_Speed() const
{
	return m_Speed;
}

//Mutator Functions
void GameObject::set_TypeID(char newTypeID)
{
	m_TypeID = newTypeID;
}

void GameObject::set_Health(int newHealth)
{
	m_Health = newHealth;
}

void GameObject::set_X(int newX)
{
	m_X = newX;
}

void GameObject::set_Y(int newY)
{
	m_Y = newY;
}

void GameObject::set_Speed(int newSpeed)
{
	m_Speed = newSpeed;
}

//Destructor
GameObject::~GameObject()
{

}
//Function Declarations
void GameObject::spawn(char type)
{
	// Leaving blank as my overloaded constructor will do the same.
}

void GameObject::draw()
{
	cout << "Type ID : " << m_TypeID << ", X = " << m_X << ", Y = " << m_Y << endl;
}


void GameObject::info()
{
	cout << "Player Info : " << endl;
}

bool GameObject::isAlive()
{
	bool alive = true;
	if (m_Health <= 0)
	{
		return false;
		m_TypeID = 'X';
		m_Speed = 0;
	}
	else
	{
		return true;
	}
}

void GameObject::update(char move, char board[MAX][MAX])
{
	
	if(GameObject::isAlive() == true)
	{ 
			if (move == 'w')
			{
				board[m_X][m_Y] = '.';
				m_X = m_X - m_Speed;
				if (board[m_X][m_Y] == ' ');
				{
					m_X = m_X +4;
				}
				board[m_X][m_Y] = m_TypeID;
				m_Health = m_Health - m_Speed;
			}
			else if (move == 's')
			{
				board[m_X][m_Y] = '.';
				m_X = m_X + m_Speed;
				if (board[m_X][m_Y] == ' ');
				{
					m_X = m_X - 4;
				}
				board[m_X][m_Y] = m_TypeID;
				m_Health = m_Health - m_Speed;
			}
			else if (move == 'd')
			{
				board[m_X][m_Y] = '.';
				m_Y = m_Y + m_Speed;
				if (board[m_X][m_Y] == ' ');
				{
					m_Y = m_Y - 4;
				}
				board[m_X][m_Y] = m_TypeID;
				m_Health = m_Health - m_Speed;
			}
			else if (move == 'a')
			{
				board[m_X][m_Y] = '.';
				m_Y = m_Y - m_Speed;
				if (board[m_X][m_Y] == ' ');
				{
					m_Y = m_Y + 4;
				}
				board[m_X][m_Y] = m_TypeID;
				m_Health = m_Health - m_Speed;
			}
	 }
	else
	{
		m_Speed = 0;
		m_TypeID = 'X';
	}

}