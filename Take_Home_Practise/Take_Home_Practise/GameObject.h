//Base Class
#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include <iostream>
#include <string>
#include <ctime>
#include <algorithm>
#define MAX 20

using namespace std;

class GameObject
{

public:
	//Default Constructor
	GameObject();

	//Overload Constuctor
	GameObject(char, int, int, int, int);

	//Accessor Functions
	char get_TypeID() const;

	int get_Health() const;

	int get_X() const;

	int get_Y() const;

	int get_Speed() const;

	//Mutator Functions
	void set_TypeID(char);

	void set_Health(int);

	void set_X(int);

	void set_Y(int);

	void set_Speed(int);

	// Destructor
	~GameObject();

	// Function Declarations
	void spawn(char type);

	void draw();

	void update(char, char[MAX][MAX]);

	void info();

	bool isAlive();



private:
	// Member variables
	char m_TypeID;
	int m_Health;
	int m_Speed; // Value between 1 & 4
	int m_X;
	int m_Y;
};

#endif