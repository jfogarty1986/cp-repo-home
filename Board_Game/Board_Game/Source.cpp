#include "Board.h"
#include "Player.h"

int main()
{
	srand((unsigned int)time(NULL));
	Board board;
	Player player,enemy;
	board.update_board(&player, &enemy);
	board.print();
	player.move();
	enemy.move();
	cout << endl;
	board.print();
	board.update_board(&player, &enemy);
	board.print();
	player.move();
	enemy.move();
	cout << endl;
	board.print();
	getchar();
	return 0;
}