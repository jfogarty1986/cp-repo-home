#pragma once
#include <ctime>
#include <algorithm>
#include <iostream>
#define MAX 10;

using namespace std;

class Player
{
public:
	Player();
	~Player();
	int get_X();
	int get_Y();
	char getID();
	void move();
private:
	int m_Health;
	int m_X;
	int m_Y;
	char m_ID;
};

