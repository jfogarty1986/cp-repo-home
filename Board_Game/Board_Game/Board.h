#pragma once
#define SIZE 10
#include "Player.h"
#include <iostream>

using namespace std;

class Board
{
public:
	Board();
	~Board();
	void print();
	void update_board(Player*,Player*);
private:
	char board[SIZE][SIZE];
};

