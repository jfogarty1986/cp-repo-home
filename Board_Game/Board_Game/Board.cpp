#include "Board.h"



Board::Board()
{
	for (int i = 0; i < SIZE; i++)
	{
		for (int j = 0; j < SIZE; j++)
		{
			board[i][j] = '.';
		}
	}
}


Board::~Board()
{
	
}

void Board::print()
{
	for (int i = 0; i < SIZE; i++)
	{
		cout << endl;
		for (int j = 0; j < SIZE; j++)
		{
			cout << "\t";
			cout << board[i][j];
		}
		cout << endl;
	}
}

void Board::update_board(Player *player,Player *enemy)
{
	board[player->get_X()][player->get_Y()] = player->getID();
	board[enemy->get_X()][enemy->get_Y()] = enemy->getID();
}