#include "Player.h"



Player::Player()
{
	m_Health = 100;
	m_X = rand() % MAX;
	m_Y = rand() % MAX;
	m_ID = 'P';
}


Player::~Player()
{
}

int Player::get_X()
{
	return m_X;
}

int Player::get_Y()
{
	return m_Y;
}

char Player::getID()
{
	return m_ID;
}

void Player::move()
{
	m_X = rand() % MAX;
	m_Y = rand() % MAX;
}