#include "Player.h"
#include "Piece.h"
#include <iostream>

using namespace std;

int main()
{
	Player player(100, 100, 100);
	Piece piece;
	piece.move(&player);

	getchar();
	return 0;
}