#ifndef PLAYER_H
#define PLAYER_H
#include <iostream>

using namespace std;

class Player
{

	friend class Piece;
public:
	Player(int, int, int);
	
private:
	int m_Health ;
	int m_Age;
	int m_ID;
	void set_hp(int);
	
};
#endif // !PLAYER_H

