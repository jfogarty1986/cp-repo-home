#include "Player.h"
Player::Player(const string& name) :
	GenericPlayer(name)
{}

Player::~Player()
{}

bool Player::IsHitting() const
{
	cout << m_Name << ", do you want a hit? (Y/N): ";
	char response;
	cin >> response;
	return (response == 'y' || response == 'Y');
}

void Player::Win(int winType) 
{
	cout << m_Name << " wins.\n";
	if (winType == 2)
	{
		m_BetAmount = m_BetAmount * 2;
		cout << m_BetAmount << " Shekels \n";
		m_Balance += m_BetAmount;
		cout << m_Name << " new balance is " << m_Balance << "\n";
	}
}

void Player::Lose() const
{
	cout << m_Name << " loses.\n";
	cout << m_Name << " new balance is " << m_Balance << "\n";
}

void Player::Push() const
{
	cout << m_Name << " pushes.\n";
}
void Player::PlaceBet() 
{
	cout << "How much does " << m_Name <<" want to bet ? ";
	//code needed to check if bet amount is valid etc
	cin >> m_BetAmount;
	m_Balance = m_Balance - m_BetAmount;
}
