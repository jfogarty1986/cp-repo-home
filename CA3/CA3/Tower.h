

#ifndef TOWER_H
#define TOWER_H

#include "Enemy.h"

class Tower : public Enemy
{
public:

	virtual ~Tower() {}

	Tower() : Enemy()
	{
		m_dyingTime = 10;
		m_health = 10;
		m_moveSpeed = 0;
		m_bulletFiringSpeed = 50;
		m_numFrames = 1;
		m_currentFrame = 1;
		m_width = 70;
		m_height = 200;
	}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health <= 8)
		{
			//TheSoundManager::Instance()->playSound("explode", 0);
			m_textureID = "damagedTower";
			//m_currentFrame = 2;
			//m_numFrames = 9;
			//m_width = 60;
			//m_height = 60;
			//m_bDying = true;
		}
		if (m_health <= 4)
		{
			//TheSoundManager::Instance()->playSound("explode", 0);

			m_textureID = "destroyedTower";
			//m_currentFrame = 1;
			//m_numFrames = 9;
			//m_width = 60;
			//m_height = 60;
			//m_bDying = true;
		}
		if (m_health <= 1)
		{
			//TheSoundManager::Instance()->playSound("explode", 0);

			m_textureID = "largeexplosion";
			m_currentFrame = 1;
			m_numFrames = 9;
			m_width = 60;
			m_height = 60;
			m_bDying = true;
		}
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			scroll(TheGame::Instance()->getScrollSpeed());

		}
		else
		{
			m_velocity.setY(0);
			m_moveSpeed = 0;
			m_textureID = "";
		}

		ShooterObject::update();
	}
private:

};

class TowerCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Tower();
	}
};


#endif


