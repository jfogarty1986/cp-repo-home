

#ifndef TREE_H
#define TREE_H

#include "Enemy.h"

class Tree : public Enemy
{
public:

	virtual ~Tree() {}

	Tree() : Enemy()
	{
		m_dyingTime = 10;
		m_health = 5;
		m_moveSpeed = 0;
		m_bulletFiringSpeed = 50;
	}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health <= 3)
		{
			//TheSoundManager::Instance()->playSound("explode", 0);

			m_textureID = "damagedTree";
			m_currentFrame = 1;
			//m_numFrames = 9;
			//m_width = 60;
			//m_height = 60;
			//m_bDying = true;
		}
		if (m_health <= 1)
		{
			//TheSoundManager::Instance()->playSound("explode", 0);

			m_textureID = "brokenTree";
			m_currentFrame = 1;
			//m_numFrames = 9;
			//m_width = 60;
			//m_height = 60;
			//m_bDying = true;
		}
		
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			scroll(TheGame::Instance()->getScrollSpeed());

		}

		ShooterObject::update();
	}
private:

};

class TreeCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Tree();
	}
};


#endif


