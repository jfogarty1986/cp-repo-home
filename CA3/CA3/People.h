

#ifndef PEOPLE_H
#define PEOPLE_H

#include "Enemy.h"

class People : public Enemy
{
public:

	virtual ~People() {}

	People() : Enemy()
	{
		m_dyingTime = 50;
		m_health = 1;
		m_moveSpeed = 1;
		m_bulletFiringSpeed = 50;
	}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health == 0)
		{
			m_textureID = "blood";
			m_currentFrame = 1;
			m_numFrames = 1;
			//m_width = 60;
			//m_height = 60;
			//m_bDying = true;
		}
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			scroll(TheGame::Instance()->getScrollSpeed());


			m_velocity.setX(-m_moveSpeed);


		}
		else
		{
			m_velocity.setY(0);
			m_moveSpeed = 0;
			//doDyingAnimation();
		}

		ShooterObject::update();
	}
private:
	bool down = false;
};

class PeopleCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new People();
	}
};


#endif


