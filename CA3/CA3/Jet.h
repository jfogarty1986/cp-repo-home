

#ifndef JET_H
#define JET_H

#include "Enemy.h"

class Jet : public Enemy
{
public:

	virtual ~Jet() {}

	Jet() : Enemy()
	{
		m_dyingTime = 50;
		m_health = 5;
		m_moveSpeed = 3;
		m_bulletFiringSpeed = 50;
	}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health == 0)
		{
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "largeexplosion";
				m_currentFrame = 0;
				m_numFrames = 9;
				m_width = 60;
				m_height = 60;
				m_bDying = true;
			}

		}
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			scroll(TheGame::Instance()->getScrollSpeed());

			//
			//if (m_bulletCounter == m_bulletFiringSpeed)
			//{
			//	TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet1", 1, Vector2D(-10, 0));
			//	TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 10, 16, 16, "bullet1", 1, Vector2D(-10, 0));
			//	//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet1", 1, Vector2D(0, 3));
			//	//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet1", 1, Vector2D(0, -3));
				m_bulletCounter = 0;
			//}
			//m_bulletCounter++;
			if (m_position.getY() < 100)
			{
				down = true;
			}
			else if (m_position.getY() > TheGame::Instance()->getGameHeight() - 200)
			{
				down = false;
			}
			if (down == true)
			{
				m_velocity.setY(m_moveSpeed);
			}
			else if (down == false)
			{
				m_velocity.setY(-m_moveSpeed);
			}
			m_velocity.setX(-m_moveSpeed);
			if (SDL_GetTicks() % 48 == 10)
			{
				TheSoundManager::Instance()->playSound("uzi", 0);
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() - 10, m_position.getY() + 10 + 10, 16, 16, "bullet1", 1, Vector2D(-10, 0));
			}
			m_bulletCounter++;

		}
		else
		{
			m_velocity.setY(0);
			doDyingAnimation();
		}

		ShooterObject::update();
	}
private:
	bool down = false;
};

class JetCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Jet();
	}
};


#endif

