

#ifndef HOUSE_H
#define HOUSE_H

#include "Enemy.h"

class house : public Enemy
{
public:

	virtual ~house() {}

	house() : Enemy()
	{
		m_dyingTime = 10;
		m_health = 10;
		m_moveSpeed = 0;
		m_bulletFiringSpeed = 50;
	}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health <= 8)
		{
			//TheSoundManager::Instance()->playSound("explode", 0);

			m_textureID = "damagedHouse";
			m_currentFrame = 1;
			//m_numFrames = 9;
			//m_width = 60;
			//m_height = 60;
			//m_bDying = true;
		}
		if (m_health <= 4)
		{
			//TheSoundManager::Instance()->playSound("explode", 0);

			m_textureID = "destroyedHouse";
			m_currentFrame = 1;
			//m_numFrames = 9;
			//m_width = 60;
			//m_height = 60;
			//m_bDying = true;
		}
		if (m_health <= 1)
		{
			//TheSoundManager::Instance()->playSound("explode", 0);

			m_textureID = "largeexplosion";
			m_currentFrame = 1;
			m_numFrames = 9;
			m_width = 60;
			m_height = 60;
			//m_bDying = true;
		}
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			scroll(TheGame::Instance()->getScrollSpeed());
			
		}

		ShooterObject::update();
	}
private:
	
};

class houseCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new house();
	}
};


#endif

