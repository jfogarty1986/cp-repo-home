

#ifndef WHITEHOUSE_H
#define WHITEHOUSE_H

#include "Enemy.h"

class WhiteHouse : public Enemy
{
public:

	virtual ~WhiteHouse() {}

	WhiteHouse() : Enemy()
	{
		m_dyingTime = 10;
		m_health = 30;
		m_moveSpeed = 0;
		m_bulletFiringSpeed = 50;
		m_numFrames = 1;
		m_currentFrame = 1;
		m_width = 70;
		m_height = 200;
	}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health <= 25)
		{
			//TheSoundManager::Instance()->playSound("explode", 0);
			m_textureID = "hitWhiteHouse";
			//m_currentFrame = 2;
			//m_numFrames = 9;
			//m_width = 60;
			//m_height = 60;
			//m_bDying = true;
		}
		if (m_health <= 18)
		{
			//TheSoundManager::Instance()->playSound("explode", 0);

			m_textureID = "damagedWhiteHouse";
			//m_currentFrame = 1;
			//m_numFrames = 9;
			//m_width = 60;
			//m_height = 60;
			//m_bDying = true;
		}
		if (m_health <= 8)
		{
			//TheSoundManager::Instance()->playSound("explode", 0);

			m_textureID = "destroyedWhiteHouse";
			//m_currentFrame = 1;
			//m_numFrames = 9;
			//m_width = 60;
			//m_height = 60;
			//m_bDying = true;
		}
		if (m_health <= 1)
		{
			//TheSoundManager::Instance()->playSound("explode", 0);

			m_textureID = "largeexplosion";
			m_currentFrame = 1;
			m_numFrames = 9;
			m_width = 60;
			m_height = 60;
			TheGame::Instance()->setLevelComplete(true);
			//m_bDying = true;
		}
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			//scroll(TheGame::Instance()->getScrollSpeed());

		}
		else
		{
			m_velocity.setY(0);
			m_moveSpeed = 0;
			m_textureID = "";
		}

		if (m_textureID == "whiteHouse" || m_textureID == "hitWhiteHouse")
		{
			if (m_bulletCounter == m_bulletFiringSpeed)
			{
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() - 10, m_position.getY() + 30, 16, 16, "bullet1", 1, Vector2D(-3, -3));
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 190, m_position.getY() + 30, 16, 16, "bullet1", 1, Vector2D(-3, -3));
				//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 95, m_position.getY() + 30, 16, 16, "bullet1", 1, Vector2D(0, -3));
				//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet1", 1, Vector2D(0, -3));
				m_bulletCounter = 0;
			}
			m_bulletCounter++;
		}
		else
		{
			if (m_bulletCounter == m_bulletFiringSpeed)
			{
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() - 10, m_position.getY() + 30, 16, 16, "bullet1", 1, Vector2D(-3, -3));
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 190, m_position.getY() + 30, 16, 16, "bullet1", 1, Vector2D(-3, -3));
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 95, m_position.getY() + 30, 16, 16, "bullet1", 1, Vector2D(0, -3));
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() - 10, m_position.getY() + 80, 16, 16, "bullet1", 1, Vector2D(-3, 0));
				m_bulletCounter = 0;
			}
			m_bulletCounter++;
		}
		
		ShooterObject::update();
	}
private:

};

class WhiteHouseCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new WhiteHouse();
	}
};


#endif



