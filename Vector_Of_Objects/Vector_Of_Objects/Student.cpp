#include "Student.h"

student::student()
{
	newGrade = ' ';
}

student::student(string name, char grade)
{
	newName = name;
	newGrade = grade;
}

student::~student()
{

}

string student::getName() const
{
	return newName;
}

char student::getGrade() const
{
	return newGrade;
}

void student::setName(string name)
{
	newName = name;
}

void student::setGrade(char grade)
{
	newGrade = grade;
}