//header file


#ifndef STUDENT_H
#define STUDENT_H
#include <iostream>
#include<string>

using namespace std;

class student
{
public:
	//Default constructor
	student();

	//Overload constructor
	student(string, char);

	//Destructor
	~student();

	//Accessor Functions
	string getName() const;
	char getGrade() const;
	
	//Mutators
	void setName(string);
	void setGrade(char);
private:
	//member variables
	string newName;
	char newGrade;
};

#endif // !STUDENT_H

