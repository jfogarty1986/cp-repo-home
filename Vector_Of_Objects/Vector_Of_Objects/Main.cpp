#include <vector>
#include <iostream>
#include<string>
#include "Student.h"

using namespace std;

void fillVector(vector<student>&);
//Fill in student info
void printVector(const vector<student>&);
//print vector  - prints info of all students

int main()
{
	vector<student> myClass;
	fillVector(myClass); 
	printVector(myClass);
	system("pause");
	return 0;
}

void fillVector(vector<student> &newMyClass)
{

	string name;
	char grade;
	cout << "How many student are in your class : ";
	int classSize;
	cin >> classSize;

	for (int i = 0; i < classSize; i++)
	{
		cout << "student name : ";
		cin >> name;
		cout << "student grade : ";
		cin >> grade;

		student newStudent(name,grade);
		newMyClass.push_back(newStudent);
		cout << endl;
	}
	cout << endl;
}

void printVector(const vector<student>& newMyClass)
{
	unsigned int size = newMyClass.size();

	for (unsigned int i = 0; i < size; i++)
	{
		cout << "Student Name : " << newMyClass[i].getName() << endl;
		cout << "Student Grade : " << newMyClass[i].getGrade() << endl;
		cout << endl;
	}
}