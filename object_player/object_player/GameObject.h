#ifndef GAMEOBJECT_H
#define GAMEOBJECT_h
#include <iostream>
using namespace std;

class GameObject  //abstract class
{
public:
	GameObject(int health = 160,int speed = 1);
	virtual void Greet() const = 0;   //pure virtual member function
	virtual void DisplayHealth() const;

protected:
	int m_Health;
	int m_Speed;
};
#endif // !GAMEOBJECT_H

