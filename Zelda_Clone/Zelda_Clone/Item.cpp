#include "Item.h"

Item::Item(string newName)
{
	name = newName;
}

void Item::setQuantity(int newQty)
{
	quantity = newQty;
}

int Item::getQuantity()
{
	return quantity;
}