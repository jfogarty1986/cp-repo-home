#ifndef OBJECT_H
#define OBJECT_H
#define MAX 70

#include <iostream>
#include <conio.h>
#include <vector>

using namespace std;
class Object
{
public:
	Object();
	int getHealth();
	int getX();
	int getY();
	void setHealth(int);
	void spawn(int,int,char[MAX][MAX]);
	void info();
	char getType();
	void setType(char);
protected:
	int health;
	int x;
	int y;
	char type;
};
#endif // !OBJECT_H

