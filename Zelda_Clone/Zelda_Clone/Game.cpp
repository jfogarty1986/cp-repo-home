#include "game.h"
Player player;

Game::Game()
{
	cout << "Welcome, we have 2 player types available for you to choose from : \n";
	cout << "1. Warrior\n";
	cout << "2. Warlock\n";
	cin >> choice;
	if (choice == 1)
	{
		Object *player = new Warrior();
	}
	else if (choice == 2)
	{
		Object *player = new Warlock();
	}
	
}

void Game::init()
{
	for (int i = 0; i < MAX; i++)
	{
		for (int j = 0; j < MAX; j++)
		{
			board[i][j] = '.';
		}
	}
	player.newPlayer();
	welcome();
	plant_trees();
	place_buildings();
}

void Game::setDifficulty()
{
	cout << "*********  DIFFICULTY  ************\n";
	cout << "1. Easy\n";
	cout << "2. Medium\n";
	cout << "3. Hard\n";
	cin >> difficulty;
	{
		if (difficulty == 1)
		{
			player.setHealth(200);
			player.setDamage(10);
			player.setLuck(5);
			player.setSkill(10);
			player.setGold(500);
		}
		else if (difficulty == 2)
		{
			player.setHealth(150);
			player.setDamage(7);
			player.setLuck(3);
			player.setSkill(7);
			player.setGold(300);
		}
		else if (difficulty == 3)
		{
			player.setHealth(200);
			player.setDamage(5);
			player.setLuck(1);
			player.setSkill(4);
			player.setGold(200);
		}
		else
		{
			setDifficulty();
		}

		cout << "Good Luck\n\n\n\n";
	}
}

void Game::welcome()
{
	cout << "Welcome" << player.getname() << endl;
	cout << "1. New Game\n";
	cout << "2. Instructions\n";
	cin >> choice;
	if (choice == 1)
	{
		newGame();
	}
	else if (choice == 2)
	{
		instructions();
	}
	system("pause");
}

void Game::instructions()
{
	cout << "Move around the map using the w, a, s & d characters. \n";
	cout << "There are various treasure chests around the map, it is up to you to find them and search through them in order to find the Lost Key Of Solitude\n";
	cout << "Be careful though, once night-time hits, you must either find shelter or use your torch\n";
	cout << "It should be noted though, your torch has a battery!!!\n";
	
	welcome();
}

void Game::newGame()
{
	setDifficulty();
}

void Game::play()
{
	print_board();
	cout << "Get Ready to spawn in \n";
	player.spawn(50,50,board);
	//board[player.getX()][player.getY()] = player.getType();
	cout << player.getType() << endl;
	system("pause");
	print_board();
	while (player.getHealth() > 0)
	{
		player.player_move(board);
		print_board();
	}
}

void Game::print_board()
{
	system("CLS");
	
		cout << "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # \n";
		for (int i = 0; i < MAX; i++)
		{
			cout << "#";
			for (int j = 0; j < MAX; j++)
			{
				cout << board[j][i] << " ";
			}
			cout << "#";
			cout << endl;
		}
		cout << "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # \n";
	
	
}

void Game::place_buildings()
{
	Shop shop1(40, 40,board,'1');
	Shop shop2(60, 60,board,'2');
	Shop shop3(20, 50,board,'3');
	buildings.push_back(shop1);
	buildings.push_back(shop2);
	buildings.push_back(shop3);
}

void Game::plant_trees()
{
	for (int i = 0; i < 15; i++)
	{
		for (int j = 0; j < 15 - i; j++)
		{
			board[i][j] = 'T';
		}
	}
}





