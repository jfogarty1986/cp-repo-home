#include "Shop.h"

Shop::Shop(int newX,int newY,char board[70][70],char newID)
{
	id = newID;
	x = newX;
	y = newY;
	board[x][y] = ' ';
	board[x - 1][y] = '#';
	board[x + 1][y] = '#';
	board[x - 1][y - 1] = '#';
	board[x + 1][y - 1] = '#';
	board[x][y - 1] = id;
	board[x][y - 2] = '#';
	board[x - 1][y - 2] = '#';
	board[x + 1][y - 2] = '#';
}