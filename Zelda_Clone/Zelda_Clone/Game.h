#ifndef GAME_H
#define GAME_H


#include "Player.h"
#include "Warlock.h"
#include "Warrior.h"
#include "Shop.h"
#include <vector>
#include <ctime>
#include <algorithm>
#include <conio.h>
#include <iostream>

using namespace std;

class Game
{
public:
	Game();
	void print_board();
	void init();
	void setDifficulty();
	void welcome();
	void instructions();
	void newGame();
	void play();
	void place_buildings();
	void plant_trees();
	
protected:
	char board[MAX][MAX];
	int difficulty;
	int choice;
	vector<Building> buildings;
};
#endif // !GAME_H

