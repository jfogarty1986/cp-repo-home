#ifndef BUILDING_H
#define BUILDING_H
#include "Object.h"
#define MAX 70

class Building : public Object
{
public:
	Building();
	int getX();
	int getY();
	void build(char[MAX][MAX]);
protected:
	int x;
	int y;
	

};
#endif // !BUILDING_H
