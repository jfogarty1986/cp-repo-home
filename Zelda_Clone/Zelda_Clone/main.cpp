#include "Game.h"

int main()
{
	srand(static_cast<unsigned int>(time(NULL))); // Seeds a random number
	Game game;
	game.init();
	game.play();
	getchar();
	return 0;
}