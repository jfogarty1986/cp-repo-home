#ifndef WARLOCK_H
#define WARLOCK_H
#include "Player.h"

class Warlock : public Player
{
public:
	Warlock();
protected:
	int mana;
};

#endif // !WARLOCK_H

