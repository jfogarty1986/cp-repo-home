#include "Object.h"

Object::Object()
{

}

int Object::getX()
{
	return x;
}
int Object::getY()
{
	return y;
}
void Object::spawn(int newX, int newY,char board[MAX][MAX])
{
	x = newX;
	y = newY;
	board[x][y] = type;
}
void Object::info()
{
	cout << "Type : " << type;
}

void Object::setHealth(int hp)
{
	health = hp;
}

char Object::getType()
{
	return type;
}
int Object::getHealth()
{
	return health;
}

void Object::setType(char newType)
{
	type = newType;
}