#ifndef PLAYER_H
#define PLAYER_H

#include "Object.h"


class Player : public Object
{
public:
	Player();
	void newPlayer();
	char getname();
	void setLuck(int);
	void setDamage(int);
	void setSkill(int);
	int getGold();
	void setGold(int);
	void player_move(char[MAX][MAX]);
	void showInventory();
	
	
protected:
	char name;
	int luck;
	int damage;
	int skill;
	char move;
	int gold;
	int weight;
	int battery_power;
	int choice;

};
#endif // !PLAYER_H

