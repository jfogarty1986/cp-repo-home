#ifndef SHOP_H
#define SHOP_H

#include "Building.h"

class Shop : public Building
{
public:
	Shop(int,int,char[70][70],char);
protected:
	char id;
};
#endif // !SHOP_H
