#include "Game.h"

Game::Game()
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			board[i][j] = '.';
		}
	}
	init();
}

void Game::init()
{
	Player player;
	Enemy enemy1('1'), enemy2('2'), enemy3('3'), enemy4('4');

	vector<GameObject> members;
	vector<GameObject>::iterator Iter;

	members.push_back(player);
	members.push_back(enemy1);
	members.push_back(enemy2);
	members.push_back(enemy3);
	members.push_back(enemy4);

	for (Iter = members.begin(); Iter != members.end(); Iter++)
	{
		Iter->draw(board);
	}
	print();
	if ((player.getX() && player.getY() == enemy1.getX() && enemy1.getY()) || (player.getX() && player.getY() == enemy2.getX() && enemy2.getY()) || (player.getX() && player.getY() == enemy3.getX() && enemy3.getY()) || (player.getX() && player.getY() == enemy4.getX() && enemy4.getY()))
	{
		battle();
	}
}

void Game::print()
{
	for (int i = 0; i < MAX; i++)
	{
		cout << "\t\t";
		for (int j = 0; j < MAX; j++)
		{
			cout << board[i][j] << ' ';
		}
		cout << endl;
	}
}

void Game::battle()
{
	cout << "Battle\n";
}