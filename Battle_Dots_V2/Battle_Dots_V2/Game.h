#ifndef GAME_H
#define GAME_h
#define MAX 10
#include "Enemy.h"
#include "Player.h"
#include <vector>
#include <iostream>

using namespace std;

class Game
{
public:
	Game();
	void init();
	void print();
	void battle();
protected:
	char board[MAX][MAX];
};
#endif // !GAME_H

