#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include <iostream>
#include <string>

using namespace std;

class GameObject
{
public:
	GameObject();
	void spawn();
	void draw(char[10][10]);
	virtual void update();
	void info();
	bool isAlive();
	int getX();
	int getY();
	char getType();
	int getSpeed();
	bool isFree();
protected:
	char m_TypeID;
	int m_Health;
	int m_Speed;
	int m_X;
	int m_Y;
};
#endif // !GAMEOBJECT_H

