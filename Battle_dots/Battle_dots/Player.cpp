#include "Player.h"
#include "Game.h"
#include <conio.h>

Player::Player()
{
	m_PSpeed = new int(rand() % 4 + 1);
	spawn();
}

void Player::update(char board[MAX][MAX])
{	
	char move;
	cout << "\t\t\t\t   Enter move : ";
	cin >> move;
	
	int new_x = 0, new_y = 0;

	if (move == 'w')
	{
		new_x = *m_PX;
		new_y = *m_PY - *m_PSpeed;
	}
	else if (move == 's')
	{
		new_x = *m_PX;
		new_y = *m_PY + *m_PSpeed;
	}
	else if (move == 'a')
	{
		new_y = *m_PY;
		new_x = *m_PX - *m_PSpeed;
	}
	else if (move == 'd')
	{
		new_y = *m_PY;
		new_x = *m_PX + *m_PSpeed;
	}
	

	if (Game::isValidMove(new_x, new_y)) {
		board[*m_PX][*m_PY] = EMPTY;
		*m_PX = new_x;
		*m_PY = new_y;
		board[*m_PX][*m_PY] = *m_PTypeID;
		*m_PHealth -= (*m_PSpeed) * 2;
		if (*m_PHealth <= 0)
		{
			*m_PHealth = 0;
			*m_PSpeed = 0;
			*m_PTypeID = EMPTY;
		}
	}
}