#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#define MAX 10
#define EMPTY '.'
#include <ctime>
#include <algorithm>
#include <iostream>
#include <vector>


using namespace std;

class GameObject
{
	
public:
	//Function Declarations
	GameObject(int health = 160,int speed  = 2);
	~GameObject();
	void spawn(char id = 'P');
	void draw();
	void info() const;
	bool isAlive();
	int getX();
	int getY();
	char getType();
	char getEmpty() const;
	void virtual update(char[MAX][MAX]);
	void check() const;
	void kill();
	int getHP();
	void setType(char);
	

protected:
	//Member variables
	char* m_PTypeID;
	int* m_PHealth;
	int* m_PSpeed;
	int* m_PX;
	int* m_PY;
};
#endif // !GAMEOBJECT_H

