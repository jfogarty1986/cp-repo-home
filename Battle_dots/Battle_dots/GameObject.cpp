#include "GameObject.h"

GameObject::GameObject(int health,int speed)
{
	m_PHealth = new int(health);
	m_PSpeed = new int(speed);
}
GameObject::~GameObject()
{
	
}
//Draw function replaces info below and prints all 
void GameObject::draw()
{
	cout << "\t\t    Player " <<  *m_PTypeID << " : " << *m_PX << ", " << *m_PY << " | Health : " << *m_PHealth << ", " << "Speed  : " << *m_PSpeed << endl;
}
void GameObject::info() const
{
	cout << "Health : " << *m_PHealth << endl;
	cout << "Speed  : " << *m_PSpeed << endl;
}
//Checks if the object health is gerater than 0
bool GameObject::isAlive()
{
	bool alive = true;
	if (m_PHealth <= 0)
	{
		alive = false;
	}
	else
	{
		alive = true;
	}
	return alive;
}
void GameObject::spawn(char id)
{
	m_PTypeID = new char(id);
	m_PX = new int(rand() % MAX);
	m_PY = new int(rand() % MAX);
}

int GameObject::getX()
{
	return *m_PX;
}

int GameObject::getY()
{
	return *m_PY;
}

char GameObject::getType()
{
	return *m_PTypeID;
}

char GameObject::getEmpty() const
{
	return '.';
}

void GameObject::update(char board[MAX][MAX])
{
	cout << "base" << endl;
}

void GameObject::kill()
{
	*m_PHealth = 0;
}

int GameObject::getHP()
{
	return *m_PHealth;
}

void GameObject::setType(char type)
{
	*m_PTypeID = type;
}