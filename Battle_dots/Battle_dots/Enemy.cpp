#include "Enemy.h"


Enemy::Enemy(char id)
{
	m_PSpeed = new int(rand() % 4 + 1);
	m_PTypeID = new char(id);
	spawn(id);
}

void Enemy::update(char board[MAX][MAX])
{
	int new_x = 0, new_y = 0;
	char up, down, left, right, move;
	up = 'w';
	down = 's';
	left = 'a';
	right = 'd';
	vector<char> moves;
	moves.push_back(up);
	moves.push_back(down);
	moves.push_back(left);
	moves.push_back(right);
	random_shuffle(moves.begin(), moves.end());
	move = moves[0];
	if (move == 'w')
	{
		new_x = *m_PX;
		new_y = *m_PY - *m_PSpeed;
	}
	else if (move == 's')
	{
		new_x = *m_PX;
		new_y = *m_PY + *m_PSpeed;
	}
	else if (move == 'a')
	{
		new_y = *m_PY;
		new_x = *m_PX - *m_PSpeed;
	}
	else if (move == 'd')
	{
		new_y = *m_PY;
		new_x = *m_PX + *m_PSpeed;
	}
	if (Game::isValidMove(new_x, new_y)) {
		board[*m_PX][*m_PY] = EMPTY;
		*m_PX = new_x;
		*m_PY = new_y;
		board[*m_PX][*m_PY] = *m_PTypeID;
		*m_PHealth -= (*m_PSpeed) * 3;
		if (*m_PHealth <= 0)
		{
			*m_PHealth = 0;
			*m_PSpeed = 0;
			*m_PTypeID = EMPTY;
		}
	}

	
}