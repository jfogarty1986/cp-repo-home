#include "Game.h"
#include <vector>

Game::Game()
{
	
}

Game::~Game()
{

}

void Game::init()
{
	//Creating the game objects
	GameObject *pPlayer = new Player();
	GameObject *pEnemy1 = new Enemy('1');
	GameObject *pEnemy2 = new Enemy('2');
	GameObject *pEnemy3 = new Enemy('3');
	GameObject *pEnemy4 = new Enemy('4');

	
	//Adding objects to the vector
	board_members.push_back(pPlayer);
	board_members.push_back(pEnemy1);
	board_members.push_back(pEnemy2);
	board_members.push_back(pEnemy3);
	board_members.push_back(pEnemy4);

	//fill the array
	fill();
	//Assign the board to the relative x and y of objects
	board[pPlayer->getX()][pPlayer->getY()] = pPlayer->getType();
	board[pEnemy1->getX()][pEnemy1->getY()] = pEnemy1->getType();
	board[pEnemy2->getX()][pEnemy2->getY()] = pEnemy2->getType();
	board[pEnemy3->getX()][pEnemy3->getY()] = pEnemy3->getType();
	board[pEnemy4->getX()][pEnemy4->getY()] = pEnemy4->getType();
	print();
}

void Game::fill()
{
	for (int i = 0; i < MAX; i++)
	{
		for (int j = 0; j < MAX; j++)
		{
			board[i][j] = '.';
		}
	}
}

void Game::print()
{
	for (int i = 0; i < MAX; i++)
	{
		cout << "\t\t\t\t";
		cout << "*";
		for (int j = 0; j < MAX; j++)
		{
			cout << board[j][i] << ' ';
		}
		cout << "*";
		cout << endl;
	}

}

void Game::battle()
{
	// temp game object
	GameObject *thePlayer = nullptr;
	for (Iter = board_members.begin(); Iter != board_members.end(); Iter++)
	{
		// Searching for the player and assigning. 
		if ((*Iter)->getType() == 'P') {
			thePlayer = (*Iter);
		}
	}
	
	// checking if the player and enemy aer on the same space
	for (Iter = board_members.begin(); Iter != board_members.end(); Iter++)
	{
		if (
			thePlayer != nullptr &&
			(*Iter)->getType() != 'P' && 
			(*Iter)->getType() != EMPTY && 
			(*Iter)->getX() == thePlayer->getX() && 
			(*Iter)->getY() == thePlayer->getY())
		{
			
			if (thePlayer->getHP() < (*Iter)->getHP())
			{
				thePlayer->kill();
			}
			else if (thePlayer->getHP() > (*Iter)->getHP())
			{
				(*Iter)->kill();
			}
			else
			{
				cout << "Equal\n";
			}
		}
	}
}

bool Game::clean()
{
	for (Iter = board_members.begin(); Iter != board_members.end();)
	{
		GameObject *object = (*Iter);
		if(!(*Iter)->isAlive()) {
			board[object->getX()][object->getY()] = EMPTY;
			if ((*Iter)->getType() == 'P')
			{
				return true;
			}

		}
		else
		{
			Iter++;
		}
	}
	return false;
}

void Game::draw()
{
	for (Iter = board_members.begin(); Iter != board_members.end(); Iter++)
	{
		(*Iter)->draw();
	}
}

void Game::info()
{
	for (Iter = board_members.begin(); Iter != board_members.end(); Iter++)
	{
		(*Iter)->info();	
	}
}

void Game::update()
{
	for (Iter = board_members.begin(); Iter != board_members.end(); Iter++)
	{
		(*Iter)->update(board);
	}
}
// checking if the move is allowed. ie, below max and above the min
bool Game::isValidMove(int x, int y)
{
	bool onBoard = true;
	if (x < 0 || y < 0 || x >= MAX || y >= MAX)
	{
		return false;
	}
	else
	{
		return true;
	}
}	
// Game is over the player is dead, returning true
bool Game::gameOver()
{	
	bool over = false;
	for (Iter = board_members.begin(); Iter != board_members.end(); Iter++)
	{
		if ((*Iter)->getType() == 'P' && (*Iter)->isAlive() == false)
		{
			over = true;
		}
	}
	return over;
}
//checking if all enemies are dead
bool Game::enemiesDead()
{
	int count = 0;
	bool dead = true;
	for (Iter = board_members.begin(); Iter != board_members.end(); Iter++)
	{
		if ((*Iter)->getType() != 'P' && (*Iter)->getHP() <= 0)
		{
			count++;
		}
	}
	if (count = 4)
	{
		return false;
	}
}

