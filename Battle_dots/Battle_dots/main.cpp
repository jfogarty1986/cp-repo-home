#include "Player.h"
#include "Game.h"
#include <ctime>
#include <algorithm>

using namespace std;

int main()
{
	srand(static_cast<unsigned int>(time(NULL))); // Seeds a random number
	Game game;
	int i = 0;
	game.init();
	bool gameOver = false;

	for (i = 0; i < 50 && game.gameOver() == false && game.enemiesDead() == false; i++)
	{
		game.draw();

		game.update();

		game.battle();

		//game.info();

		gameOver = game.clean();
		
		game.print();

		if (game.gameOver() == true)
		{
			break;
		}
	}	
	
	if (i <= 50)
	{
		cout << "You Win!!!!\n";
	}
	else
	{
		cout << "you lost\n";
	}
	system("pause");
	getchar();
	return 0;
}