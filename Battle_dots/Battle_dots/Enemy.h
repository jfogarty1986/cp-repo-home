#ifndef ENEMY_H
#define ENEMY_H
#define MAX 10
#include "Game.h"

#include "GameObject.h"

class Enemy : public GameObject
{
public:
	Enemy(char id);
	~Enemy();
	void virtual update(char[MAX][MAX]);
private:

};
#endif // !ENEMY_H

