#ifndef PLAYER_H
#define PLAYER_H
#define MAX 10
#include "GameObject.h"

class Player : public GameObject
{
public:
	Player();
	~Player();
	void move();
	void virtual update(char[MAX][MAX]);
private:
};
#endif // !PLAYER_H

