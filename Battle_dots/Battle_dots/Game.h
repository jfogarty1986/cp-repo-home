#ifndef GAME_H
#define GAME_H


#include "Enemy.h"
#include "Player.h"
#include "GameObject.h"


class Game
{
public:
	void init();
	void draw();
	bool clean();
	void battle();
	void info();
	void update();
	void fill();
	void print();
	bool gameOver();
	bool enemiesDead();
	static bool isValidMove(int, int);
	Game();
	~Game();
private:
	char board[MAX][MAX];
	vector<GameObject*> board_members;
	vector<GameObject*>::iterator Iter;
	
};
#endif // !GAME_H