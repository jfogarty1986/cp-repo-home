#include "Object.h"

Object::Object(int &x, int &y, int &hp)
{
	m_Health = hp;
	m_X = x;
	m_Y = y;
}

int Object::get_X()
{
	return m_X;
}

int Object::get_Y()
{
	return m_Y;
}
