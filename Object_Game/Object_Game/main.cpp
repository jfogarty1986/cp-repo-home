#include "Object.h"

using namespace std;

void deduct_hp(int*);

int main()
{
	int x = 10, y = 10, hp = 160;
	int *p_X = &x, *p_Y = &y, *p_Hp = &hp;
	Object player(*p_X, *p_Y, *p_Hp);
	cout << " X before function call = :" << x << endl;
	cout << "PX before function call = : " << p_X << endl;
	cout << "Value PX before function call : " << *p_X << endl;
	deduct_hp(&x);
	cout << " X after function call = :" << x << endl;
	cout << "PX X after function call = : " << p_X << endl;
	cout << "Value PX X after function call : " << *p_X << endl;
	

	getchar();
	return 0;
}

void deduct_hp(int *x)
{
	cout << "Mem Loc inside function : " << &x << endl;
	cout << "X inside function: " << x << endl;
	*x = 5;
}


