#ifndef OBJECT_H
#define OBJECT_H
#include <iostream>
#include <string>

using namespace std;

class Object
{
public:
	Object::Object(int&,int&,int&);
	int Object::get_X();
	int Object::get_Y();
private:
	int m_Health;
	int m_X;
	int m_Y;
	
};
#endif // !OBJECT_H

