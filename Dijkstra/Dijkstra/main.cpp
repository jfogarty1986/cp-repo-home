#include "Graph.h"
#include <vector>
#include <list>
#include <algorithm>
#include <iostream>

using namespace std;
int main()
{
	Graph graph(5);
	graph.addEdge(0, 2, 0);
	graph.addEdge(2, 1, 2);
	graph.addEdge(3, 2, 4);
	graph.addEdge(4, 3, 4);
	graph.addEdge(4, 2, 3);
	//graph.print_graph();
	
	list<struct edge> edgeList = graph.getEdges(4);
	list<struct edge>::iterator Iter;

	for (Iter = edgeList.begin(); Iter != edgeList.end(); Iter++)
	{
		cout << Iter->from << " " << Iter->to << " " << Iter->weight << endl;
	}

	getchar();
	return 0;
}