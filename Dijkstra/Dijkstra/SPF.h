#pragma once
#include "Graph.h"
#include <limits>



class SPF 
{
public:
	SPF(int,Graph);
	int **getTree();
	
private:
	Graph graph(int);
	int size;
	bool *known;
	int *dist;
	int *previous;
	int source;
	bool isEveryNodeKnown();
};
