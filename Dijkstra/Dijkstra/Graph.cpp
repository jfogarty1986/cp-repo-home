#include "Graph.h"

using namespace std;

Graph::Graph(int newSize)
{
	size = newSize;
	edges = new list< struct edge >[size];
}

Graph::Graph(const Graph &otherGraph) {
	size = otherGraph.size;
	edges = otherGraph.edges;
}

void Graph::addEdge(int from, int to, int weight)
{
	struct edge newEdge;
	newEdge.from = from;
	newEdge.to = to;
	newEdge.weight = weight;

	edges[from].push_back(newEdge);
}

void Graph::print_graph()
{
	for (int i = 0; i < size; i++)
	{
		list<struct edge> edgeList = edges[i];
		list<struct edge>::iterator Iter;

		for (Iter = edgeList.begin(); Iter != edgeList.end(); Iter++)
		{
			cout << Iter->from << " " << Iter->to << " " << Iter->weight  << endl;
		}
	}
}

list<struct edge> Graph::getEdges(int from)
{
	return edges[from];
}