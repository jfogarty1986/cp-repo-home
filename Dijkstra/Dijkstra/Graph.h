#pragma once
#include <vector>
#include <list>
#include <algorithm>
#include <iostream>
struct edge
{
	int from, to, weight;
};

class Graph
{
public:
	Graph(int);
	Graph(const Graph&);
	void addEdge(int,int,int);
	void print_graph();
	std::list<struct edge> getEdges(int);
	int size;
private:
	std::list< struct edge > *edges;
	
};

