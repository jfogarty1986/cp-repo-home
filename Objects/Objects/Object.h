#ifndef OBJECT_H
#define OBJECT_H
#include <iostream>
using namespace std;

class Object
{
public:
	Object();
	int get_health();
private:
	int m_health;
	int m_X;
	int m_Y;
};


#endif

