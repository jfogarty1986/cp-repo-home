#ifndef PLAYER_H
#define PLAYER_H
#include <iostream>
#include "Object.h"

using namespace std;

class Player
{
public:
	Player(Object player);

private:
	int get_health();
};

#endif