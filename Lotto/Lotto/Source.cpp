#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>

using namespace std;

int main()
{
	int choice = 0;
	int random_numbers[6], random_lucky_stars[2];
	const int MAX_NUM = 49, LUCKY_STARS = 11;
	
	do
	{
		vector <int> numbers;
		vector <int> stars;
		cout << "Welcome to lotto number creator\n\n";
		cout << "1. Irish Lotto\n";
		cout << "2. Euro millions\n";
		cout << "3. Quit\n";
		cin >> choice;

		srand(time(NULL));

		for (unsigned int i = 0; i < MAX_NUM; i++)
		{
			numbers.push_back(i + 1);
		}

		for (unsigned int i = 0; i < LUCKY_STARS; i++)
		{
			stars.push_back(i + 1);
		}
		cout << "b4 shuffle\n";
		for (unsigned int i = 0; i < MAX_NUM; i++) {
			cout << numbers[i] << "\n";
		}

		std::random_shuffle(numbers.begin(), numbers.end());

		cout << "after shuffle \n";
		for (unsigned int i = 0; i < MAX_NUM; i++) {
			cout << numbers[i] << "\n";
		}

		if (choice == 1)
		{
			cout << " Irish Lottery numbers are as follows : \n";
			for (int i = 0; i < 6; i++)
			{
				random_numbers[i] = numbers[i];
				cout << "Number " << i + 1 << " is : " << random_numbers[i] << "\n";
			}
			cout << "\n\n\n";
		}
		else if (choice == 2)
		{
			std::random_shuffle(stars.begin(), stars.end());
			for (int i = 0; i < 6; i++)
			{
				random_numbers[i] = numbers[i];
				random_lucky_stars[i] = stars[i];
				cout << "Number " << i + 1 << " is : " << random_numbers[i] << "\n";
			}
			cout << "Lucky stars : " << random_lucky_stars[0] << " & " << random_lucky_stars[1];
			cout << "\n\n\n";
		}
		else
		{
			cout << "Goodbye" << endl;
		}
	} while (choice != 3);



	return 0;
}

