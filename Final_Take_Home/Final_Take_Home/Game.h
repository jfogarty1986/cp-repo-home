#ifndef GAME_H
#define GAME_H
#include "GameObject.h"
#include "Enemy.h"
#include "Player.h"
#define MAX 10

using namespace std;

class Game
{
public:
	Game();
	~Game();
	void start();
private:
	char board[MAX][MAX];
};
#endif // !GAME_H

