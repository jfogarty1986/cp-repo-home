#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include <iostream>
#include <string>

using namespace std;

class GameObject
{
	friend class Player;
	friend class Enemy;
public:
	GameObject();
	~GameObject();
	void spawn();
	void draw();
	void info();
	bool isAlive();
	
protected:
	char m_typeID;
	int m_health;
	int m_speed;
	int m_x;
	int m_y;
};

#endif // !GAMEOBJECT_H