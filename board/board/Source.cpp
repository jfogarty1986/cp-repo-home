//James Fogarty

#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <vector>
#include <string>
#include <conio.h>
#define EMPTY '.'

int random_speed();

using namespace std;


int main()
{
	int player_health = 160, enemy_1_health = 160, enemy_2_health = 160, enemy_3_health = 160, enemy_4_health = 160;
	const int BOARD_SIZE = 23;
	char board[23][23];

	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			board[i][j] = EMPTY;
		}
		cout << "\n";
	}


	cout << "\n\n\n\n";

	srand(static_cast<unsigned int>(time(NULL))); // Seeds a random number
	int player_spawn_x = rand() % 23 + 1;
	int player_spawn_y = rand() % 23 + 1;
	int enemy1_spawn_x = rand() % 23 + 1;
	int enemy1_spawn_y = rand() % 23 + 1;
	int enemy2_spawn_x = rand() % 23 + 1;
	int enemy2_spawn_y = rand() % 23 + 1;
	int enemy3_spawn_x = rand() % 23 + 1;
	int enemy3_spawn_y = rand() % 23 + 1;
	int enemy4_spawn_x = rand() % 23 + 1;
	int enemy4_spawn_y = rand() % 23 + 1;

	board[player_spawn_x][player_spawn_y] = 'P';
	board[enemy1_spawn_x][enemy1_spawn_y] = '1';
	board[enemy2_spawn_x][enemy2_spawn_y] = '2';
	board[enemy3_spawn_x][enemy3_spawn_y] = '3';
	board[enemy4_spawn_x][enemy4_spawn_y] = '4';

	for (int i = 0; i < BOARD_SIZE; i++)
	{
		cout << "\t\t";
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			std::cout << board[i][j] << "  ";
		}
		cout << "\n";
	}
	cout << "Player Health : \n";

	bool game_over = false;
	char move;
	while (!game_over)
	{
		int speed = random_speed();
		cout << "Enter move ( w/a/s/d ) : ";
		move = _getch();
		cout << endl;
		int x = 0, y = 0;
		switch (move)
		{
		case 'w':
			x = player_spawn_x;
			y = player_spawn_y;
			board[player_spawn_x][player_spawn_y] = EMPTY;
			board[x - speed][y] = 'P';
			player_spawn_x = x - speed;
			player_health = player_health - speed * 2;
			break;
		case 's':
			x = player_spawn_x;
			y = player_spawn_y;
			board[player_spawn_x][player_spawn_y] = EMPTY;
			board[x + speed][y] = 'P';
			player_spawn_x = x + speed;
			player_health = player_health - speed * 2;
			break;
		case 'a':
			x = player_spawn_x;
			y = player_spawn_y;
			board[player_spawn_x][player_spawn_y] = EMPTY;
			board[x][y - speed] = 'P';
			player_spawn_y = y - speed;
			player_health = player_health - speed * 2;
			break;
		case 'd':
			x = player_spawn_x;
			y = player_spawn_y;
			board[player_spawn_x][player_spawn_y] = EMPTY;
			board[x][y + speed] = 'P';
			player_spawn_y = y + speed;
			player_health = player_health - speed * 2;
			break;
		}

		for (int i = 0; i < BOARD_SIZE; i++)
		{
			cout << "\t\t";
			for (int j = 0; j < BOARD_SIZE; j++)
			{
				std::cout << board[i][j] << "  ";
			}
			cout << "\n";
		}
		cout << "Player Health : " << player_health << endl;
		cout << "Player Speed : " << speed << endl;
	}

	system("pause");

	return 0;
}

int random_speed()
{
	int speed = rand() % 4 + 1;
	return speed;
}


