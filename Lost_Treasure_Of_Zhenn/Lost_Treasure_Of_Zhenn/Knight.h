#ifndef KNIGHT_H
#define KNIGHT_H
#include "Player.h"

class Knight : public Player
{
public:
	Knight();
	int virtual attack();
protected:
	int stamina;
};
#endif // !KNIGHT_H

