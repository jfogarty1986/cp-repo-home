#ifndef CHARACTER_H
#define CHARACTER_H
#include "GameObject.h"
#include <conio.h>

class Character : public GameObject
{
public:
	Character();
	~Character();
	bool isDead();
	int getHP();
	char getID();
	void kill();
	int virtual attack();
	void virtual generateDialogue();
	void virtual greet();
	bool virtual hasTreasure();
	void deductHP(int);
	int virtual getGold();
	void virtual setGold(int);
	void increaseHP();
	int getMapID();
	void setHealthPotions(int);
	void setManaPotions(int);
	int virtual getHealthPotions();
	int virtual getManaPotions();
protected:
	int gold;
	int mapID;
	int hp;
	bool dead = false;
	int powerOfHit;
	char currentMove;
	bool hasItem = false;
	int healthPotions,manaPotions, bronzeSword,silverSword,goldSword;

};
#endif // !CHARACTER_H

