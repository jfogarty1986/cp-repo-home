#ifndef ENEMY_H
#define ENEMY_H
#include "Character.h"
#include <string>

class Enemy : public Character
{
public:
	Enemy(int,int,bool);
	Enemy();
	int virtual attack();
	void generateNames();
	int attackPower();
	int virtual getGold();
	bool virtual hasTreasure();
protected:
	string enemyNames[10] = {"James","Gregor","Burba","Traore","Hobbit","Preth","jambon","Achilles","Thobald","Thedor"};
	string name;
	int attacks[4] = { 5,10,20,40 };
	int hit;
	int treasure = 0;
	bool hasItem = false;
};
#endif 

