#ifndef GAME_H
#define GAME_H
#include "Warlock.h"
#include "Ogre.h"
#include "Knight.h"
#include "GameObject.h"
#include "Player.h"
#include "Shop.h"
#include "Goblin.h"
#include "Grogon.h"
#include "Casino.h"

class Game
{
public:
	Game();
	~Game();
	void init();
	void battle(Character*);
	void createObjects();
	void check();
	void update();
	void print();
	void clear();
	void move();
	bool gameOver = false;
	int choice;
	void secret();
	int gold_dropped;
	void useItem(int);
	void welcome();
	void gameEnd();
protected:
	int x, y;
	vector<Character*> mapPlayers;
	vector<Character*>::iterator Iter;
	Character *player;
	Shop *shop1 = new Shop(50, 20);
	Shop *shop2 = new Shop(60, 60);
	Shop *shop3 = new Shop(20, 40);
	vector<Shop*> shops;
	vector<Shop*>::iterator Iter2;
	vector<std::pair <int, int>> coords;
	int playerHit, Enemyhit;
	
	//int uniqueCords[40][40];
	int new_x;
	int new_y;
	Bag bag;
	int tempx, tempy;
	char currentMove;
	int enemy_temp_x, enemy_temp_y;
	char map[MAX][MAX];
	Grogon grogon;
	int useID;
	char yesNo;
	int treasures = 0;
	bool secretActive = false;
	Casino *hilo = new Casino(45, 68);

};
#endif // !GAME_H

