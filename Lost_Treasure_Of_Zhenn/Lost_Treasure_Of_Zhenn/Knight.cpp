#include "Knight.h"
Knight::Knight()
{
	stamina = 300;
}

int Knight::attack()
{
	cout << "Your turn\n";
	pause();
	cout << " Stamina ->" << stamina << endl;
	if (stamina == 0)
	{
	cout << "Out of stamina!!!!!\n";
	cout << "Press 1 to punch the enemy\n";
	cin >> choice;
	powerOfHit = 4;
	}
	cout << "1. Leg Sweep -> Cost 5 Stamina -> Damage 15\n";
	cout << "2. Torso Shot -> Cost 10 Stamina -> Damage 25\n";
	cout << "3. Head Shot -> Cost 20 Stamina -> Damage 50\n";
	cin >> choice;
	if (choice == 1)
	{
		cout << "You strike the enemy, sweeping him off his feet\n";
		powerOfHit = 15;
		stamina -= 5;
	}
	else if (choice == 2)
	{
		cout << "Your enemy shudders in pain as your sword pierces his torso\n";
		powerOfHit = 25;
		stamina -= 10;
	}
	else if (choice == 3)
	{
		cout << "Your attack slices the enemys' cheek open, exposing his solid cheek bone, to which the base of your sword smashes\n";
		powerOfHit = 50;
		stamina -= 20;
	}
	else
	{
		cout << "Swing & a miss\n";
		powerOfHit = 0;
	}
	return powerOfHit;
}