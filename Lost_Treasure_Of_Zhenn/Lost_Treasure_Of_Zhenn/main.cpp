#include "Player.h"
#include "Enemy.h"
#include "game.h"
#include <Windows.h>
#include "MMSystem.h"

using namespace std;


int main()
{
	srand(static_cast<unsigned int>(time(NULL))); // Seeds a random number
	//PlaySound(TEXT("music.wav"), NULL, SND_SYNC);
	Game game;
	game.init();
	game.createObjects();
	while (game.gameOver == false)
	{
		game.print();
		game.move();
		game.check();
		//game.clear();
	}
	getchar();
	return 0;
}

