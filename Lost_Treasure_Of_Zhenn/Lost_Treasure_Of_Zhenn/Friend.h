#ifndef FRIEND_H
#define FRIEND_H
#include "Character.h"

class Friend : public Character
{
public:
	Friend();
	void virtual greet();
protected:
};
#endif // !FRIEND_H

