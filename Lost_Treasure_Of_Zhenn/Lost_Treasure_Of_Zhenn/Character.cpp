#include "Character.h"

Character::Character()
{

}

Character::~Character()
{

}

bool Character::isDead()
{
	if (hp <= 0)
	{
		kill();
	}
	return dead;
}

int Character::getHP()
{
	return hp;
}
void Character::kill()
{
	
}

int Character::attack() 
{
	cout << "Character attack\n";
	return 0;
}

char Character::getID()
{
	return id;
}

void Character::generateDialogue()
{
	cout << "Base Dialogue\n";
}

void Character::deductHP(int hit)
{
	hp = hp - hit;
}

void Character::greet()
{
	cout << "Hi, i'm a character\n";
}
/*
char Character::move(char map[MAX][MAX])
{
	cin >> currentMove;
	map[x][y] = '.';
	int new_x = 0, new_y = 0;

	if (currentMove == 'w')
	{
		new_x = x;
		new_y = y - 1;
	}
	else if (currentMove == 's')
	{
		new_x = x;
		new_y = y + 1;
	}
	else if (currentMove == 'a')
	{
		new_y = y;
		new_x = x - 1;
	}
	else if (currentMove == 'd')
	{
		new_y = y;
		new_x = x + 1;
	}
	x = new_x;
	y = new_y;
	map[x][y] = id;
	return currentMove;
}
*/

int Character::getGold()
{
	return gold;
}

void Character::setGold(int g)
{
	gold += g;
}

void Character::increaseHP()
{
	hp += 60;
	if (hp > 100)
	{
		hp = 100;
	}
}

int Character::getMapID()
{
	return mapID;
}

bool Character::hasTreasure()
{
	return hasItem;
}

void Character::setHealthPotions(int potions)
{
	healthPotions += potions;
}
void Character::setManaPotions(int potions)
{
	manaPotions += potions;
}
int Character::getHealthPotions()
{
	return healthPotions;
}
int Character::getManaPotions()
{
	return healthPotions;
}