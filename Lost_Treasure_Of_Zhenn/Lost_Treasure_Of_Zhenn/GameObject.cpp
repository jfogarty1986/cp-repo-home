#include "GameObject.h"
//Abstract class, the only functions utilised in here are the getX function and getY function
GameObject::GameObject()
{

}

GameObject::~GameObject()
{

}

int GameObject::getX()
{
	return x;
}
int GameObject::getY()
{
	return y;
}
void GameObject::setX(int newX)
{
	x = newX;
}
void GameObject::setY(int newY)
{
	y = newY;
}

char GameObject::getID()
{
	return id;
}