#include "Grogon.h"

Grogon::Grogon()
{
	hp = 350;
	id = 'X';
	x = 35;
	y = 35;
	mapID = 4;
}

int Grogon::attack()
{
	cout << "Grogon lunges at you ";
	powerOfHit = rand() % 50;
	if (powerOfHit <= 10)
	{
		cout << "catching your arm with his tail causing " << powerOfHit << " damage\n";
	}
	else if (powerOfHit > 10 && powerOfHit <= 30)
	{
		cout << "catching your legs, knocking you on the ground causing " << powerOfHit << " damage\n";
	}
	else if (powerOfHit > 30 && powerOfHit <= 45)
	{
		cout << "catching your head, slicing your face open from ear to ear, exposing the fragile flesh underneath causing " << powerOfHit << " damage\n";
	}
	else
	{
		cout << "Causing a critical strike, gauging one of your eyes out, causing " << powerOfHit << " damage and causing you to miss a turn\n";
	}
	return powerOfHit;
}

void Grogon::spawn(char map[MAX][MAX])
{
	x = 35;
	y = 35;
	map[x][y] = id;
}