#include "Warlock.h"

Warlock::Warlock()
{
	mana = 100;
	manaPotions = 2;
	healthPotions = 2;
}

int Warlock::getMana()
{
	return mana;
}

int Warlock::attack()
{
	cout << "Your turn\n";
	pause();
	cout << " Mana ->" << mana << endl;
	if (mana == 0)
	{
		cout << "Out of mana!!!!!\n";
		cout << "Press 1 to hit with your stick\n";
		cin >> choice;
		powerOfHit = 4;
	}
	cout << "1. Lightning -> Cost 5 Mana -> Damage 10\n";
	cout << "2. Fire -> Cost 10 Mana -> Damage 20\n";
	cout << "3. Ice -> Cost 20 Mana -> Damage 40\n";
	cout << "4. Restore HP -> Cost 30 Mana -> Restores 50% of HP\n";
	cin >> choice;
	if (choice == 1)
	{
		cout << "A poweful bolt of lightning hits the enemy\n";
		powerOfHit = 10;
		mana -= 5;
	}
	else if (choice == 2)
	{
		cout << "You summon molten fireballs from the depths\n";
		powerOfHit = 20;
		mana -= 10;
	}
	else if (choice == 3)
	{
		cout << "You send icy chiils swirling around the enemy\n";
		powerOfHit = 40;
		mana -= 20;
	}
	else if (choice == 4)
	{
		cout << "You summon the healing gods around you, bringing forth the zen spheres of gretol\n";
		hp += 50;
		if (hp > 100)
		{
			hp = 100;
		}
		mana -= 30;
	}
	else
	{
		cout << "Your magic misses\n";
		powerOfHit = 0;
	}
	return powerOfHit;
}
//
//void Warlock::setHealthPotions(int potions)
//{
//	health_potions += potions;
//}
//
//void Warlock::setManaPotions(int potions)
//{
//	manaPotions += potions;
//}
//int Warlock::getHealthPotions()
//{
//	return healthPotions;
//}
//int Warlock::getManaPotions()
//{
//	return healthPotions;
//}
//


