#ifndef PLAYER_H
#define PLAYER_H
#include "Character.h"

class Player : public Character
{
public:
	Player();
	int virtual attack();
	int choice;
	char currentMove;
	void pause();
	int virtual getGold();
	/*int virtual getHealthPotions();
	int virtual getManaPotions();
	void virtual setHealthPotions(int);
	void virtual setManaPotions(int);*/
	void virtual setGold(int);
protected:
	int health_potions = 2;
};
#endif // !PLAYER_H

