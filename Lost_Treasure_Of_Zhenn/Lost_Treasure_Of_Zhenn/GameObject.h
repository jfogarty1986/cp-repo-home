#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include <iostream>
#include <vector>
#include <ctime>
#include <algorithm>
#define MAX 70

using namespace std;

class GameObject
{
public:
	GameObject();
	~GameObject();
	int getX();
	int getY();
	void setX(int);
	void setY(int);
	char getID();
protected:
	int x;
	int y;
	char id;
	
};
#endif // !GAMEOBJECT_H

