#ifndef WARLOCK_H
#define WARLOCK_H
#include "Player.h"

class Warlock : public Player
{
public:
	Warlock();
	int getMana();
	int virtual attack();
	/*void setHealthPotions(int);
	void setManaPotions(int);
	int virtual getHealthPotions();
	int virtual getManaPotions();*/
protected:
	int mana;

};
#endif // !WARLOCK_H

