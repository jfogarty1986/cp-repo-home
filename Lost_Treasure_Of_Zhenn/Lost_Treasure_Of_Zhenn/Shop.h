#include "Building.h"
#include "Player.h"

class Shop : public Building
{
public:
	Shop(int,int);
	void restock();
	void viewItems(Character*);
	void open(Character*);
private:
	int choice;
	int qty;
	int total;
	int health_potion = 10;
	int mana_potion = 10;
	int bronze_sword = 5;
	int silver_sword = 5;
	int gold_sword = 5;
};
