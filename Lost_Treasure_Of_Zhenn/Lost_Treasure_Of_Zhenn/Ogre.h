#ifndef OGRE_H
#define OGRE_H
#include "Enemy.h"

class Ogre : public Enemy
{
public:
	Ogre(int,int,bool);
	int virtual attack();
	void virtual generateDialogue();
	
protected:
	vector<int> attacks;
	vector<string> dialogue;
	string dialogue1, dialogue2, dialogue3;
	string sentence;
	
};
#endif // !OGRE_H

