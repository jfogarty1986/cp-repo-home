#ifndef GOBLIN_H
#define GOBLIN_H
#include "Enemy.h"

class Goblin : public Enemy
{
public:
	Goblin(int,int,bool);
	int virtual attack();
	int virtual getGold();
	void virtual generateDialogue();
protected:
	vector<int> attacks;
	vector<string> dialogue;
	string dialogue1, dialogue2, dialogue3;
	string sentence;
};
#endif // !GOBLIN_H

