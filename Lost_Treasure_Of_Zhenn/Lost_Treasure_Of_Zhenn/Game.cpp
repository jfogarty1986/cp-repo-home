#include "game.h"

Game::Game()
{
	
}

void Game::init()
{
	//Display welcome message
	welcome();
	
	//Prompt user to choose a class
	cout << "Game Initialised\n";
	cout << "Choose a class\n";
	cout << "1. Warlock\n";
	cout << "2. Knight\n";
	cin >> choice;
	//initialising the new class based on players choice of character
	if (choice == 1)
	{
		player = new Warlock();
	}
	else if (choice == 2)
	{
		player = new Knight();
	}
	
	//setting all array tiles to .
	for (int i = 0; i < MAX; i++)
	{
		for (int j = 0; j < MAX; j++)
		{
			map[i][j] = '.';
		}
	}
}
void Game::battle(Character *enemy)
{
	//Clears the screen to have a full battle screen
	system("CLS");
	bool battleOver = false;
	while (!battleOver)
	{
		//system("CLS");
		//Printing the player & enemy health
		cout << "Your Health : " << player->getHP() << endl;
		cout << "Enemy Health : " << enemy->getHP() << endl;
		//Deducting the hp from both player & enemy depending on the attack
		player->deductHP(enemy->attack());
		enemy->deductHP(player->attack());
		if (player->getHP() <= 0)
		{
			gameEnd();
			//if the player dies, game is over
			gameOver = true;
			battleOver = true;
			//delete player;
		}
		else if (enemy->getHP() <= 0)
		{
			//after enemy is killed, gold is dropped, gold is random between 0 and 50
			//Gold is then added to players balance by calling the setGold() function
			cout << "You have killed the enemy\n";
			gold_dropped = rand() % 50;
			cout << "Enemy has dropped " << gold_dropped << " gold ";
			player->setGold(gold_dropped);
			cout << ", you have picked it up. You now have " << player->getGold() << " gold\n";
			if (enemy->hasTreasure() == true)
			{
				cout << "You have found 1 of the missing pieces from the treasure of Zhenn\n";
				treasures++;
			}
			//prompting user to press any key to continue
			system("pause");
			//setting battle over to true so to break from loop
			battleOver = true;
			delete enemy;
		}
	}
}

void Game::check() 
{
	//assigning a temp x and y value to the players x and y values
	tempx = player->getX();
	tempy = player->getY();
	for (Iter = mapPlayers.begin(); Iter != mapPlayers.end(); Iter++)
	{
		//comparing the map id values. An ogre is of type 3 and a troll is of type 2
		if ((*Iter)->getMapID() == 3 || (*Iter)->getMapID() == 2)
		{
			if ((*Iter)->getX() == tempx && (*Iter)->getY() == tempy)
			{
				//if the x and y coordinates match, a battle will occur between the two. Code then breaks to the battle fucntion
				cout << "BATTLE\n";
				battle(*Iter);
			}
		}
	}
	//comparing the x and y coords to the coords of the shops. 
	for (Iter2 = shops.begin(); Iter2 != shops.end(); Iter2++)
	{
		if ((*Iter2)->getX() == tempx && (*Iter2)->getY() == tempy)
		{
			//sending in the object player to the shop so balance etc can be retrieved
			(*Iter2)->viewItems(player);
		}
	}
	if (treasures == 2)
	{
		grogon.spawn(map);
		system("CLS");
		cout << "You hear a thundering crack from the depth below\n";
		cout << "I AM GROGON, master of the fire & brimstone, come and face me!!! you know where to find me\n";
		cout << "Grogon is a powerful god, be careful and make sure you have enough health & mana potions before making your way to the X\n";
		system("pause");
	}

	if (grogon.isDead())
	{
		//trigger the end of the game dialogue
		system("CLS");
		cout << "Well done young adventurer\n";
		cout << "You have restored the Lost Treasures back to where they should be\n";
		cout << "Go now, and be proud of what you have achieved today\n";
		gameOver = true;
	}
	if(tempx == 69)
	{
		//if the players x value reaches 69, the player will then be able to view all the enemies on the map. Initially this was for debugging purposes but have left it in as a secret
		for (Iter = mapPlayers.begin(); Iter != mapPlayers.end(); Iter++)
		{
			if ((*Iter)->getMapID() == 3 || (*Iter)->getMapID() == 2)
			{
				map[(*Iter)->getX()][(*Iter)->getY()] = 'E';
			}
		}
	}
	//comparing the x and y coords to the coords of the casino
	if (tempx == hilo->getX() && tempy == hilo->getY())
	{
		hilo->welcome(player);
	}

}
void Game::update()
{
	move();
}
void Game::print()
{
	system("CLS");
	//cout << player->getX();
	//cout << player->getY();
	
	for (int i = 0; i < MAX; i++)
	{
		for (int j = 0; j < MAX; j++)
		{
			cout << map[j][i] << " ";
		}
		cout << endl;
	}
}

Game::~Game()
{

}

void Game::createObjects()
{
	/*
	coords.push_back(make_pair(50, 20));
	coords.push_back(make_pair(60, 60));
	coords.push_back(make_pair(20, 20));
	coords.push_back(make_pair(player->getX(), player->getY()));
	
	for (int i = 0; i < 11; i++)
	{
		enemy_temp_x = rand() % 70;
		enemy_temp_y = rand() % 70;
		Character *ogre = new Ogre(enemy_temp_x, enemy_temp_y);
		if(coords[i] == )
		coords.push_back(make_pair(enemy_temp_x, enemy_temp_y));
		enemy_temp_x = rand() % 70;
		enemy_temp_y = rand() % 70;
		Character *goblin = new Goblin(enemy_temp_x, enemy_temp_y);
		coords.push_back(make_pair(enemy_temp_x, enemy_temp_y));
		mapPlayers.push_back(ogre);
		mapPlayers.push_back(goblin);
	}*/
	
	Character *ogre1 = new Ogre(49,49,true);
	Character *ogre2 = new Ogre(1, 25, false);
	Character *ogre3 = new Ogre(25, 25, false);
	Character *ogre4 = new Ogre(7, 38, false);
	Character *ogre5 = new Ogre(50, 10, false);
	Character *ogre6 = new Ogre(55,62, false);
	Character *ogre7 = new Ogre(68, 51, false);
	Character *ogre8 = new Ogre(5, 52, false);
	Character *ogre9 = new Ogre(35, 11, false);
	Character *ogre10 = new Ogre(55, 3, false);
	Character *ogre11 = new Ogre(65, 14, false);
	Character *ogre12 = new Ogre(40, 60, false);
	Character *goblin1 = new Goblin(45, 45, true);
	Character *goblin2 = new Goblin(64, 60, false);
	Character *goblin3 = new Goblin(62, 20, false);
	Character *goblin4 = new Goblin(45, 20, false);
	Character *goblin5 = new Goblin(5, 45, false);
	Character *goblin6 = new Goblin(15, 34, false);
	Character *goblin7 = new Goblin(34, 50, false);
	Character *goblin8 = new Goblin(30, 30, false);
	Character *goblin9 = new Goblin(50, 10, false);
	Character *goblin10 = new Goblin(12, 61, false);
	Character *goblin11 = new Goblin(13, 41, false);
	Character *goblin12 = new Goblin(60, 40, false);

	// on creation of a thing, add pair of coords to a static vector of coords
	// on creation of next thing, check if generated coords are in vector
	// if not, give next thing coords and add them to vector
	
	mapPlayers.push_back(ogre1);
	mapPlayers.push_back(ogre2);
	mapPlayers.push_back(ogre3);
	mapPlayers.push_back(ogre4);
	mapPlayers.push_back(ogre5);
	mapPlayers.push_back(ogre6);
	mapPlayers.push_back(ogre7);
	mapPlayers.push_back(ogre8);
	mapPlayers.push_back(ogre9);
	mapPlayers.push_back(ogre10);
	mapPlayers.push_back(ogre11);
	mapPlayers.push_back(ogre12);
	mapPlayers.push_back(player);
	mapPlayers.push_back(goblin1);
	mapPlayers.push_back(goblin2);
	mapPlayers.push_back(goblin3);
	mapPlayers.push_back(goblin4);
	mapPlayers.push_back(goblin5);
	mapPlayers.push_back(goblin6);
	mapPlayers.push_back(goblin7);
	mapPlayers.push_back(goblin8);
	mapPlayers.push_back(goblin9);
	mapPlayers.push_back(goblin10);
	mapPlayers.push_back(goblin11);
	mapPlayers.push_back(goblin12);

	for (Iter = mapPlayers.begin(); Iter != mapPlayers.end(); Iter++)
	{
		map[(*Iter)->getX()][(*Iter)->getY()] = (*Iter)->getID();
	}
	/*
	map[player->getX()][player->getY()] = player->getID();
	map[goblin1->getX()][goblin1->getY()] = goblin1->getID();
	map[ogre1->getX()][ogre1->getY()] = ogre1->getID();*/
	
	shop1->build(map);
	shop2->build(map);
	shop3->build(map);
	hilo->build(map);

	shops.push_back(shop1);
	shops.push_back(shop2);
	shops.push_back(shop3);

	
}

void Game::clear()
{
	//clears all enemies on the map, not being used. For testing purposes
	for (Iter = mapPlayers.begin(); Iter != mapPlayers.end(); Iter++)
	{
		if ((*Iter)->getID() == 'E')
		{
			delete (*Iter);
		}
	}
}

//player movement
void Game::move()
{

	x = player->getX();
	y = player->getY();
	cin >> currentMove;
	map[x][y] = '.';
	new_x = 0, new_y = 0;

	if (currentMove == 'w')
	{
		new_x = x;
		new_y = y - 1;	
	}
	else if (currentMove == 's')
	{
		new_x = x;
		new_y = y + 1;
	}
	else if (currentMove == 'a')
	{
		new_y = y;
		new_x = x - 1;
	}
	else if (currentMove == 'd')
	{
		new_y = y;
		new_x = x + 1;
	}
	else if (currentMove == 'i')
	{
		new_x = x;
		new_y = y;
		system("CLS");
	}
	x = new_x;
	y = new_y;
	player->setX(new_x);
	player->setY(new_y);
	map[x][y] = player->getID();

	shop1->build(map);
	shop2->build(map);
	shop3->build(map);
}

void Game::useItem(int id)
{
	switch (id)
	{
	case 1:
		player->increaseHP();
		
	}
}

void Game::welcome()
{
	cout << "Welcome to The Lost Treasure of Zhenn\n";
	cout << "There are 2 lost pieces of the Zhenn sphere, you must retrieve them young adventurer\n";
	cout << "They were taken by an ogre and a goblin into the beyond\n";
	cout << "If they aren't retrieved our Princess will die\n";
	cout << "Now hurry along! \n";
	cout << "1. instructions\n";
	cout << "2. Play\n";
	cin >> choice;
	if (choice == 1)
	{
		cout << "Move with w,a,s and d. Follow on screen instructions to battle opponents\n";
		cout << "Shops are visible by the # symbols\n";
		cout << "Casino is visible by the * symbols\n";
		system("pause");
	}

}

void Game::gameEnd()
{
	system("CLS");
	cout << "You have failed in your jourey, better luck next time\n";
	system("pause");
}


