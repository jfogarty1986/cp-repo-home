#ifndef CASINO_H
#define CASINO_H
#include <iostream>
#include <algorithm>
#include <ctime>
#include "Character.h"

class Casino
{
public:
	Casino(int,int);
	~Casino();
	int getX();
	int getY();
	void build(char map[70][70]);
	void welcome(Character *);
	void hilo();
	void lowRoller();
	int roll();
	bool results(int,char);
	void check(char);
private:
	int x, y, number, balance, winnings,dice,dice1,dice2,total,lastTotal,stake;
	char choice,hOrL,playAgain;
	bool lose = false;
	Character *gambler;
};
#endif // !CASINO_H

