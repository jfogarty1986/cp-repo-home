#include "Building.h"

Building::Building()
{
	id = '#';
}

void Building::build(char map[MAX][MAX])
{
	map[x][y] = ' ';
	map[x - 1][y] = '#';
	map[x + 1][y] = '#';
	map[x - 1][y - 1] = '#';
	map[x + 1][y - 1] = '#';
	map[x][y - 1] = id;
	map[x][y - 2] = '#';
	map[x - 1][y - 2] = '#';
	map[x + 1][y - 2] = '#';
}

