#ifndef PLAYER_H
#define PLAYER_H
#include "Object.h"

class Player : public Object
{
public:
	Player();
	void move();
protected:
};
#endif // !PLAYER_H

