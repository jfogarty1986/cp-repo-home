#ifndef ENEMY_H
#define ENEMY_H
#include "Object.h"

class Enemy : public Object
{
public:
	Enemy();
	void move();
protected:
};
#endif // !ENEMY_H

