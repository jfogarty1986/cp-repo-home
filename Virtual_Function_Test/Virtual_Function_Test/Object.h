#ifndef OBJECT_H
#define OBJECT_H
#include<iostream>

using namespace std;

class Object
{
public:
	Object();
	virtual void move();
protected:
	int m_Health;
	int m_X;
	int m_Y;
};
#endif // !OBJECT_H
