#include "Player.h"
#include "Enemy.h"
#include <vector>

int main()
{
	Object *pPlayer = new Player();
	Object *pEnemy = new Enemy();

	vector<Object> members;
	vector<Object>::iterator Iter;

	members.push_back(*pPlayer);
	members.push_back(*pEnemy);

	for (Iter = members.begin(); Iter != members.end(); Iter++)
	{
		(Iter)->move();
	}
	getchar();
	return 0;
}